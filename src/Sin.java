// 313285942

import java.util.Map;

/**
 * The type Sin.
 */
public class Sin extends UnaryExpression {
    /**
     * Instantiates a new Sin.
     *
     * @param expression the expression
     */
    public Sin(Expression expression) {
        super(expression);
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        return Math.sin(Math.toRadians(super.getExpression().evaluate(assignment)));
    }

    @Override
    public double evaluate() throws Exception {
        return Math.sin(Math.toRadians(super.getExpression().evaluate()));
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return new Sin(super.getExpression().assign(var, expression));
    }

    @Override
    public Expression differentiate(String var) {
        if (this.isVariableInExpression(var)) {
            // (sin (f)' = cos (f) * f'
            return new Mult(new Cos(super.getExpression()), super.getExpression().differentiate(var));
        } else {
            return new Num(0.0);
        }
    }

    @Override
    public Expression simplify() {
        try {
            return new Num(this.evaluate());
        } catch (Exception e) {
            return new Sin(super.getExpression().simplify());
        }
    }

    @Override
    public String toString() {
        return "sin(" + super.getExpression().toString() + ")";
    }
}
