// 313285942

import java.util.Map;

/**
 * The type Pow.
 * Implemented with two expressions, second is the exponent.
 */
public class Pow extends BinaryExpression {
    /**
     * Instantiates a new Pow.
     *
     * @param expression1 the expression 1
     * @param expression2 the expression 2
     */
    public Pow(Expression expression1, Expression expression2) {
        super(expression1, expression2);
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        double res = Math.pow(super.getExpression1().evaluate(assignment), super.getExpression2().evaluate(assignment));
        if (Double.isNaN(res)) {
            throw new Exception("Action is not mathematically valid");
        }
        return res;
    }

    @Override
    public double evaluate() throws Exception {
        double res = Math.pow(super.getExpression1().evaluate(), super.getExpression2().evaluate());
        if (Double.isNaN(res)) {
            throw new Exception("Action is not mathematically valid");
        }
        return res;
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return new Pow(super.getExpression1().assign(var, expression), super.getExpression2().assign(var, expression));
    }

    @Override
    public Expression differentiate(String var) {
        // (f^g)' = f^g * (f' * (g / f) + g' * ln(f))
        Expression f = super.getExpression1();
        Expression g = super.getExpression2();
        Pow pow = new Pow(f, g);
        Mult mult1 = new Mult(f.differentiate(var), new Div(g, f));
        Mult mult2 = new Mult(g.differentiate(var), new Log(new Const("e", Math.E), f));
        Plus plus = new Plus(mult1, mult2);
        return new Mult(pow, plus);
    }

    @Override
    public Expression simplify() {
        try {
            return new Num(this.evaluate());
        } catch (Exception e) {
            return new Pow(super.getExpression1().simplify(), super.getExpression2().simplify());
        }
    }

    @Override
    public String toString() {
        return "(" + super.getExpression1() + "^" + super.getExpression2() + ")";
    }
}
