// 313285942

import java.util.Map;

/**
 * The type Log.
 * Implemented with two expressions, first expression is the base.
 */
public class Log extends BinaryExpression {
    /**
     * Instantiates a new Log.
     *
     * @param expression1 the expression 1
     * @param expression2 the expression 2
     */
    public Log(Expression expression1, Expression expression2) {
        super(expression1, expression2);
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        double res = Math.log(super.getExpression2().evaluate(assignment))
                   / Math.log(super.getExpression1().evaluate(assignment));
        if (Double.isNaN(res) || Double.isInfinite(res)) {
            throw new Exception("Action is not mathematically valid");
        }
        return res;
    }

    @Override
    public double evaluate() throws Exception {
        double res = Math.log(super.getExpression2().evaluate())
                / Math.log(super.getExpression1().evaluate());
        if (Double.isNaN(res) || Double.isInfinite(res)) {
            throw new Exception("Action is not mathematically valid");
        }
        return res;
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return new Log(super.getExpression1().assign(var, expression), super.getExpression2().assign(var, expression));
    }

    @Override
    public Expression differentiate(String var) {
        // log(f, g) = (((log f  * g') / g) - (f' * log g) / f))) / (log f)^2
        Expression f = super.getExpression1();
        Expression g = super.getExpression2();
        Div numerator1 = new Div(new Mult(new Log(new Const("e", Math.E), f), g.differentiate(var)), g);
        Div numerator2 = new Div(new Mult(f.differentiate(var), new Log(new Const("e", Math.E), g)), f);
        Minus sumNumerator = new Minus(numerator1, numerator2);
        Pow denominator = new Pow(new Log(new Const("e", Math.E), f), new Num(2.0));
        return new Div(sumNumerator, denominator);
    }

    @Override
    public Expression simplify() {
        Expression simplified1 = super.getExpression1().simplify();
        Expression simplified2 = super.getExpression2().simplify();
        try {
            return new Num(this.evaluate());
        } catch (Exception e) {
            // log(X, X) = 1
            if (simplified1.toString().equals(simplified2.toString())) {
                return new Num(1.0);
            }
            return new Log(simplified1, simplified2);
        }
    }
    @Override
    public String toString() {
        return "log(" + super.getExpression1() + ", " + super.getExpression2() + ")";
    }
}
