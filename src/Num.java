// 313285942

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The type Num. implemented by a double.
 */
public class Num implements Expression {
    private double num;

    /**
     * Instantiates a new Num.
     *
     * @param num the num
     */
    public Num(double num) {
        this.num = num;
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        return this.num;
    }

    @Override
    public double evaluate() throws Exception {
        return this.num;
    }

    @Override
    public List<String> getVariables() {
        return new ArrayList<String>();
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return new Num(this.num);
    }

    @Override
    public Expression differentiate(String var) {
        // the differentiate of a number is always 0.
        return new Num(0.0);
    }

    @Override
    public Expression simplify() {
        return new Num(this.num);
    }

    @Override
    public String toString() {
        return String.valueOf(this.num);
    }
}
