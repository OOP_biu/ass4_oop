// 313285942

import java.util.Map;

/**
 * The type Neg.
 */
public class Neg extends UnaryExpression {
    /**
     * Instantiates a new Neg.
     *
     * @param expression the expression
     */
    public Neg(Expression expression) {
        super(expression);
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        return -super.getExpression().evaluate(assignment);
    }

    @Override
    public double evaluate() throws Exception {
        return -super.getExpression().evaluate();
    }

    @Override
    public Expression assign(String var, Expression e) {
        return  new Neg(super.getExpression().assign(var, e));
    }

    @Override
    public Expression differentiate(String var) {
        // the differentiate of a var is it's coefficient. a Neg object's coefficient is always 11.
        return new Neg(super.getExpression().differentiate(var));
    }

    @Override
    public Expression simplify() {
        try {
            return new Num(this.evaluate());
        } catch (Exception e) {
            return new Neg(super.getExpression().simplify());
        }
    }

    @Override
    public String toString() {
        return "(-" + super.getExpression() + ")";
    }
}
