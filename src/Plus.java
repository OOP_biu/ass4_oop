// 313285942

import java.util.Map;

/**
 * The type Plus.
 */
public class Plus extends BinaryExpression {
    /**
     * Instantiates a new Plus.
     *
     * @param expression1 the expression 1
     * @param expression2 the expression 2
     */
    public Plus(Expression expression1, Expression expression2) {
        super(expression1, expression2);
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        return super.getExpression1().evaluate(assignment) + super.getExpression2().evaluate(assignment);
    }

    @Override
    public double evaluate() throws Exception {
        double res = super.getExpression1().evaluate() + super.getExpression2().evaluate();
        if (Double.isNaN(res) || Double.isInfinite(res)) {
            throw new Exception("Action is mathematically forbidden");
        }
        return res;
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return new Plus(super.getExpression1().assign(var, expression), super.getExpression2().assign(var, expression));
    }

    @Override
    public Expression differentiate(String var) {
        // (f + g)' = f' + g'
        return new Plus(super.getExpression1().differentiate(var), super.getExpression2().differentiate(var));
    }

    @Override
    public Expression simplify() {
        Expression simplified1 = super.getExpression1().simplify();
        Expression simplified2 = super.getExpression2().simplify();
        try {
            return new Num(this.evaluate());
        } catch (Exception e) {
            System.out.print("");
        }
        /*
        if exception was thrown, then one of the expression has variables.
        we still need to check if one of the expressions has no variables and it's value is 0.
        if yes, we can simplify the expression by this rule: X + 0 = X.
        */
        try {
            if (simplified1.evaluate() == 0) {
                return simplified2;
            }
        } catch (Exception ex) {
            System.out.print("");
        }
        try {
            if (simplified2.evaluate() == 0) {
                return simplified1;
            }
        } catch (Exception exception) {
            System.out.print("");
        }
        return new Plus(simplified1, simplified2);
    }

    @Override
    public String toString() {
        return "(" + super.getExpression1() + " + " + super.getExpression2() + ")";
    }
}
