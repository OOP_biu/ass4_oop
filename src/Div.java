// 313285942

import java.util.Map;

/**
 * The type Div.
 */
public class Div extends BinaryExpression {
    /**
     * Instantiates a new Div.
     *
     * @param expression1 the expression 1
     * @param expression2 the expression 2
     */
    public Div(Expression expression1, Expression expression2) {
        super(expression1, expression2);
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        double res1 = super.getExpression1().evaluate(assignment);
        double res2 = super.getExpression2().evaluate(assignment);
        if (res2 == 0.0) {
            throw new Exception("Division by zero is invalid");
        }
        return res1 / res2;
    }

    @Override
    public double evaluate() throws Exception {
        double res1 = super.getExpression1().evaluate();
        double res2 = super.getExpression2().evaluate();
        if (res2 == 0.0) {
            throw new Exception("Division by zero is invalid");
        }
        return res1 / res2;
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return new Div(super.getExpression1().assign(var, expression), super.getExpression2().assign(var, expression));
    }

    @Override
    public Expression differentiate(String var) {
        // (f / g)' = ((f' * g) - (f * g') / g^2)
        Mult factor1 = new Mult(super.getExpression1().differentiate(var), super.getExpression2());
        Mult factor2 = new Mult(super.getExpression2().differentiate(var), super.getExpression1());
        Minus numerator = new Minus(factor1, factor2);
        Pow denominator = new Pow(super.getExpression2(), new Num(2.0));
        return new Div(numerator, denominator);
    }

    @Override
    public Expression simplify() {
        Expression simplified1 = super.getExpression1().simplify();
        Expression simplified2 = super.getExpression2().simplify();
        try {
            return new Num(this.evaluate());
        } catch (Exception e) {
            System.out.print("");
        }
        /*
        if exception was thrown, then one of the expression has variables.
        we still need to check for another simplifications.
        */
        try {
            // X / 1 = X
            if (simplified2.evaluate() == 1) {
                return simplified1;
            }
        } catch (Exception ex) {
            System.out.print("");
        }
        try {
            // 0 / X = 0
            if (simplified1.evaluate() == 0) {
                return new Num(0.0);
            }
        } catch (Exception exception) {
            System.out.print("");
        }
        // X / X = 1
        if (simplified1.toString().equals(simplified2.toString())) {
            return new Num(1.0);
        }
        // default
        return new Div(simplified1, simplified2);
    }

    @Override
    public String toString() {
        return "(" + super.getExpression1() + " / " + super.getExpression2() + ")";
    }
}
