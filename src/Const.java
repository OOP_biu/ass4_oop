// 313285942

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The type Const. implemented by a string (representing the constant) and a double (representing the value).
 */
public class Const implements Expression {
    private String constant;
    private double value;

    /**
     * Instantiates a new Const.
     *
     * @param constant the constant
     * @param value    the value
     */
    public Const(String constant, double value) {
        this.constant = constant;
        this.value = value;
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        return this.value;
    }

    @Override
    public double evaluate() throws Exception {
        return this.value;
    }

    @Override
    public List<String> getVariables() {
        return new ArrayList<String>();
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return this;
    }

    @Override
    public Expression differentiate(String var) {
        // the differentiate of a number is always 0, and a constant is a number.
        return new Num(0.0);
    }

    @Override
    public Expression simplify() {
        return new Const(this.constant, this.value);
    }

    @Override
    public String toString() {
        return this.constant;
    }
}
