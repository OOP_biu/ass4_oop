// 313285942

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The type Var. implemented by a string representing the variable.
 */
public class Var implements Expression {
    private String variable;

    /**
     * Instantiates a new Var.
     *
     * @param var the variable
     */
    public Var(String var) {
        this.variable = var;
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        if (assignment.containsKey(this.variable)) {
            return assignment.get(this.variable);
        }
        throw new Exception("The variable has no assignment");
    }

    @Override
    public double evaluate() throws Exception {
        throw new Exception("The variable has no assignment");
    }

    @Override
    public List<String> getVariables() {
        List<String> variables = new ArrayList<>();
        variables.add(this.variable);
        return variables;
    }

    @Override
    public Expression assign(String var, Expression expression) {
        if (var.equals(this.variable)) {
            return expression;
        }
        return this;
    }

    @Override
    public Expression differentiate(String var) {
        if (var.equals(this.variable)) {
            // the differentiate of a var is it's coefficient. a Var object's coefficient is always 1.
            return new Num(1.0);
        } else {
            // the differentiate of a constant is always 0.
            return new Num(0.0);
        }
    }

    @Override
    public Expression simplify() {
        return new Var(this.variable);
    }

    @Override
    public String toString() {
        return this.variable;
    }
}
