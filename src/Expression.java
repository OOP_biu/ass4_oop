// 313285942

import java.util.List;
import java.util.Map;

/**
 * The interface Expression.
 */
public interface Expression {
    /**
     * Evaluate an expression with a specific assignment.
     *
     * @param assignment the assignment
     * @return the result
     * @throws Exception the exception
     */
    double evaluate(Map<String, Double> assignment) throws Exception;
    /**
     * Evaluate an expression.
     *
     * @return the result
     * @throws Exception the exception
     */
    double evaluate() throws Exception;
    /**
     * Gets variables of an expression.
     *
     * @return the variables
     */
    List<String> getVariables();
    /**
     * convert expression to a string representation.
     * @return the string
     */
    String toString();
    /**
     * Assign a variable to expression.
     *
     * @param var        the var
     * @param expression the expression
     * @return the new expression
     */
    Expression assign(String var, Expression expression);
    /**
     * Differentiate expression by a given variable.
     *
     * @param var the variable
     * @return the expression
     */
    Expression differentiate(String var);
    /**
     * Simplify expression.
     *
     * @return the simplified expression
     */
    Expression simplify();
}
