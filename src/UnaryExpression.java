// 313285942

import java.util.List;

/**
 * The type Unary expression.
 */
public abstract class UnaryExpression extends BaseExpression {
    private Expression expression;

    /**
     * Instantiates a new Unary expression.
     *
     * @param expression the expression
     */
    public UnaryExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public List<String> getVariables() {
        return this.expression.getVariables();
    }

    /**
     * Checks if variable is in expression.
     *
     * @param var       the variable
     * @return true if in expression, false if not.
     */
    public boolean isVariableInExpression(String var) {
        List<String> variables = this.getVariables();
        for (String v : variables) {
            if (v.equals(var)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets expression.
     *
     * @return the expression
     */
    protected Expression getExpression() {
        return expression;
    }
}
