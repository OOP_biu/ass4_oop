// 313285942

import java.util.Map;

/**
 * The type Mult.
 */
public class Mult extends BinaryExpression {
    /**
     * Instantiates a new Mult.
     *
     * @param expression1 the expression 1
     * @param expression2 the expression 2
     */
    public Mult(Expression expression1, Expression expression2) {
        super(expression1, expression2);
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        return super.getExpression1().evaluate(assignment) * super.getExpression2().evaluate(assignment);
    }

    @Override
    public double evaluate() throws Exception {
        return super.getExpression1().evaluate() * super.getExpression2().evaluate();
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return new Mult(super.getExpression1().assign(var, expression), super.getExpression2().assign(var, expression));
    }

    @Override
    public Expression differentiate(String var) {
        // (f * g)' = (f' * g) + (f * g')
        Mult factor1 = new Mult(super.getExpression1().differentiate(var), super.getExpression2());
        Mult factor2 = new Mult(super.getExpression1(), super.getExpression2().differentiate(var));
        return new Plus(factor1, factor2);
    }

    @Override
    public Expression simplify() {
        Expression simplified1 = super.getExpression1().simplify();
        Expression simplified2 = super.getExpression2().simplify();
        try {
            return new Num(this.evaluate());
        } catch (Exception e) {
            System.out.print("");
        }
        /*
        if exception was thrown, then one of the expression has variables.
        we still need to check for another simplifications.
        */
        try {
            // X * 0 = 0
            if (simplified1.evaluate() == 0) {
                return new Num(0.0);
            }
            // X * 1 = X
            if (simplified1.evaluate() == 1) {
                return simplified2;
            }
        } catch (Exception ex) {
            System.out.print("");
        }
        try {
            // X * 0 = 0
            if (simplified2.evaluate() == 0) {
                return new Num(0.0);
            }
            // X * 1 = X
            if (simplified2.evaluate() == 1) {
                return simplified1;
            }
        } catch (Exception exception) {
            System.out.print("");
        }
        return new Mult(simplified1, simplified2);
    }

    @Override
    public String toString() {
        return "(" + super.getExpression1() + " * " + super.getExpression2() + ")";
    }
}
