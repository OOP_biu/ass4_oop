// 313285942

import java.util.List;

/**
 * The type Binary expression.
 */
public abstract class BinaryExpression extends BaseExpression {
    private Expression expression1;
    private Expression expression2;

    /**
     * Instantiates a new Binary expression.
     *
     * @param expression1 the expression 1
     * @param expression2 the expression 2
     */
    public BinaryExpression(Expression expression1, Expression expression2) {
        this.expression1 = expression1;
        this.expression2 = expression2;
    }

    @Override
    public List<String> getVariables() {
        List<String> variables = this.expression1.getVariables();
        variables.addAll(this.expression2.getVariables());
        return variables;
    }

    /**
     * Checks if variable is in expression.
     *
     * @param var the variable
     * @return true if in expression, false if not.
     */
    public boolean isVariableInExpression(String var) {
        List<String> variables = this.getVariables();
        for (String v : variables) {
            if (v.equals(var)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets expression 1.
     *
     * @return the expression 1
     */
    protected Expression getExpression1() {
        return expression1;
    }

    /**
     * Gets expression 2.
     *
     * @return the expression 2
     */
    protected Expression getExpression2() {
        return expression2;
    }
}
