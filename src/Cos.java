// 313285942

import java.util.Map;

/**
 * The type Cos.
 */
public class Cos extends UnaryExpression {
    /**
     * Instantiates a new Cos.
     *
     * @param expression the expression
     */
    public Cos(Expression expression) {
        super(expression);
    }

    @Override
    public double evaluate(Map<String, Double> assignment) throws Exception {
        return Math.cos(Math.toRadians(super.getExpression().evaluate(assignment)));
    }

    @Override
    public double evaluate() throws Exception {
        return Math.cos(Math.toRadians(super.getExpression().evaluate()));
    }

    @Override
    public Expression assign(String var, Expression expression) {
        return new Cos(super.getExpression().assign(var, expression));
    }

    @Override
    public Expression differentiate(String var) {
        if (this.isVariableInExpression(var)) {
            // (cos (f))' = - sin (f) * f'
            return new Neg(new Mult(new Sin(super.getExpression()), super.getExpression().differentiate(var)));
        } else {
            // the differentiate of a constant is always 0.
            return new Num(0.0);
        }
    }

    @Override
    public Expression simplify() {
        try {
            return new Num(this.evaluate());
        } catch (Exception e) {
            return new Cos(super.getExpression().simplify());
        }
    }

    @Override
    public String toString() {
        return "cos(" + super.getExpression().toString() + ")";
    }
}
