// 313285942

import java.util.HashMap;
import java.util.Map;

/**
 * The class tests the expressions.
 */
public class ExpressionsTest {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Expression expression = new Plus(
                                    new Plus(
                                            new Mult(new Num(2.0), new Var("x")),
                                            new Sin(new Mult(new Num(4.0), new Var("y")))
                                            ), new Pow(new Const("e", Math.E), new Var("x")));
        System.out.println(expression);
        Map<String, Double> assignment = new HashMap<>();
        assignment.put("x", 2.0);
        assignment.put("y", 0.25);
        try {
            System.out.println(expression.evaluate(assignment));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(expression.differentiate("x"));
        try {
            System.out.println(expression.differentiate("x").evaluate(assignment));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println(expression.differentiate("x").simplify());
    }
}
